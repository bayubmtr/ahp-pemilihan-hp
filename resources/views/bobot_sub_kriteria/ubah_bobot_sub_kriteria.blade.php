@extends('layouts.app')

@section('content')

<div class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
        <div class="col-sm-6">
            <h1 class="m-0 text-dark">UBAH BOBOT SUB KRITERIA</h1>
        </div>
        <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="/">Home</a></li>
            <li class="breadcrumb-item"><a href="/bobot_kriteria">Bobot Sub Kriteria</a></li>
            <li class="breadcrumb-item active">Ubah Bobot Sub Kriteria</li>
            </ol>
        </div>
        </div>
    </div>
</div>

<section class="content">
    <div class="card">
    @include ('includes.flash')
    <div class="card-body">
        <form role="form" method="post" action="{{ route('bobot_sub_kriteria.store') }}">
            @csrf
            <div class="card-body">
                <div class="row">
                    <div class="col-xl-2 col-lg-3 col-md-4 col-sm-12">
                    <div class="form-group">
                        <label for="exampleInputEmail1">Kriteria Pertama</label>
                        @php
                        @endphp
                        <input type="hidden" class="form-control" value="{{$data1->id}}" name="id_sub_kriteria_1" readonly>
                        <input type="text" class="form-control" value="{{$data1->nama}}" readonly>
                    </div>
                    </div>
                    <div class="col-xl-2 col-lg-3 col-md-4 col-sm-12">
                    <div class="form-group">
                        <label for="exampleInputEmail1">Lebih Penting</label>
                        <select class="form-control" name="nilai">
                        @foreach (prioritas() as $item)
                            <option {{ $item == 1 ? 'selected' : '' }} value="{{ $item }}">{{ $item }}x lebih penting dari</option>
                        @endforeach
                        </select>
                    </div>
                    </div>
                    <div class="col-xl-2 col-lg-3 col-md-4 col-sm-12">
                    <div class="form-group">
                        <label for="exampleInputPassword1">Sub Kriteria Kedua</label>
                        <input type="hidden" class="form-control" value="{{$data2->id}}" name="id_sub_kriteria_2" readonly>
                        <input type="text" class="form-control" value="{{$data2->nama}}" readonly>
                    </div>
                </div>
            </div>
            <div class="card-footer">
                <button type="submit" class="btn btn-primary">Simpan</button>
            </div>
            </form>
        </div>
    </div>
</section>
@include ('includes.script')
@endsection
