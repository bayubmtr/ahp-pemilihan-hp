<aside class="main-sidebar sidebar-dark-primary elevation-4">
    <a href="{{ route('home') }}" class="brand-link">
      {{-- <img src="/logo_small.png" alt="Logo" class="brand-image img-circle elevation-3"
           style="opacity: .8"> --}}
      <span class="brand-text font-weight-light">SPK Smartphone</span>
    </a>
    <div class="sidebar">
        <div class="user-panel mt-3 pb-3 mb-3 d-flex">
            <div class="image">
                <img src="/dist/img/user2-160x160.jpg" class="img-circle elevation-2" alt="User Image">
            </div>
            <div class="info">
                <a href="{{ route('akun.index') }}" class="d-block">{{ Auth::user()->name }}</a>
            </div>
        </div>

        <!-- Sidebar Menu -->
        <nav class="mt-2">
            <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">

                @if(auth()->user()->role == 'admin')
                <li class="nav-item has-treeview {{ Request::is('kriteria', 'alternatif') ? 'menu-open' : '' }}">
                    <a href="#" class="nav-link {{ Request::is('kriteria', 'alternatif') ? 'active' : '' }}">
                        <i class="nav-icon fa fa-pie-chart"></i>
                        <p>
                            MASTER DATA
                            <i class="right fa fa-angle-left"></i>
                        </p>
                    </a>
                    <ul class="nav nav-treeview pl-4">
                        <li class="nav-item">
                            <a href="{{ route('kriteria.index') }}" class="nav-link {{ Request::is('kriteria') ? 'active' : '' }}">
                            <p>KRITERIA</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{ route('alternatif.index') }}" class="nav-link {{ Request::is('alternatif') ? 'active' : '' }}">
                            <p>ALTERNATIF</p>
                            </a>
                        </li>
                    </ul>
                </li>
                <li class="nav-item has-treeview {{ Request::is('bobot_kriteria', 'bobot_sub_kriteria*') ? 'menu-open' : '' }}">
                    <a href="#" class="nav-link {{ Request::is('bobot_kriteria', 'bobot_sub_kriteria*') ? 'active' : '' }}">
                        <i class="nav-icon fa fa-tree"></i>
                        <p>
                            ANALISA
                            <i class="fa fa-angle-left right"></i>
                        </p>
                    </a>
                    <ul class="nav nav-treeview pl-4">
                        <li class="nav-item">
                            <a href="{{ route('bobot_kriteria.index') }}" class="nav-link {{ Request::is('bobot_kriteria') ? 'active' : '' }}">
                            <p>BOBOT KRITERIA</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{ route('bobot_sub_kriteria.index') }}" class="nav-link {{ Request::is('bobot_sub_kriteria*') ? 'active' : '' }}">
                            <p>BOBOT SUB KRITERIA</p>
                            </a>
                        </li>
                    </ul>
                </li>
                <li class="nav-item">
                    <a href="{{ route('hasil.index') }}" class="nav-link {{ Request::is('hasil*') ? 'active' : '' }}">
                        <i class="nav-icon fa fa-calculator"></i>
                        <p>
                            HASIL
                        </p>
                    </a>
                </li>
                @else
                <li class="nav-item">
                    <a href="{{ route('bobot_kriteria.index') }}" class="nav-link {{ Request::is('bobot_kriteria') ? 'active' : '' }}">
                        <i class="nav-icon fa fa-user"></i>
                        <p>
                            BOBOT KRITERIA
                        </p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="{{ route('hasil.index') }}" class="nav-link {{ Request::is('hasil') ? 'active' : '' }}">
                        <i class="nav-icon fa fa-user"></i>
                        <p>
                            HASIL
                        </p>
                    </a>
                </li>
                @endif
                <li class="nav-item">
                    <a href="#" class="nav-link"
                        onclick="event.preventDefault();
                        document.getElementById('logout-form').submit();">
                        <i class="nav-icon fa fa-sign-out"></i>
                        <p>
                            LOGOUT
                        </p>
                    </a>
                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                        @csrf
                    </form>
                </li>
            </ul>
        </nav>
        <!-- /.sidebar-menu -->
    </div>
</aside>
