<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Models\User;
use Redirect;

class AkunController extends Controller
{
    public function index()
    {
        $data = auth()->user();
        return view('akun.index')->with('data', $data);
    }
    public function store(Request $request)
    {
        $this->validate($request,[
            'password' => 'required|string|min:6',
            'password_confirmation' => 'required|same:password',
         ]);

        $akun = auth()->user();
        $akun->password = bcrypt($request->password);
        $akun->save();
        return redirect()->route('akun.index')->with('success',
        'Berhasil mengubah password');
    }
}
