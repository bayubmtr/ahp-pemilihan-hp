<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Models\Alternatif;
use App\Models\NilaiAlternatif;
use App\Models\Kriteria;

class AlternatifController extends Controller
{
    public function index()
    {
        $kriteria = Kriteria::all();
        $data = Alternatif::all();
        return view('alternatif.alternatif')->with('data', $data);
    }
    public function show($id)
    {
        $data = Alternatif::with('nilai_alternatif.sub_kriteria.kriteria')->where('id', $id)->first();
        if($data){
            return view('alternatif.nilai_alternatif')->with('data', $data);
        }
    }
    public function create()
    {
        $kriteria = Kriteria::with('sub_kriteria')->get();
        return view('alternatif.tambah_alternatif')->with('kriteria', $kriteria);
    }
    public function store(Request $request)
    {
        $this->validate($request,[
            'nama' => 'required|unique:alternatif,nama',
         ]);

        $data = new Alternatif;
        $data->nama = request('nama');
        $data->save();
        if(isset($request->nilai)){
            foreach (request('nilai') as $key => $value) {
                $data_nilai = new NilaiAlternatif;
                $data_nilai->id_alternatif = $data->id;
                $data_nilai->id_sub_kriteria = $value;
                $data_nilai->save();
            }
        }
        return redirect()->route('alternatif.index')->with('success',
        'Berhasil menambah data');
    }
    public function edit($id)
    {
        $alternatif = Alternatif::with('nilai_alternatif')->where('id', $id)->first();
        $data = Kriteria::with('sub_kriteria')->get();
        if($data){
            return view('alternatif.edit_alternatif')->with(['data' => $data, 'alternatif' => $alternatif]);
        }
        return redirect()->route('alternatif.index');
    }
    public function update(Request $request, $id)
    {
        $this->validate($request,[
            'nama' => 'required'
         ]);

        $data = Alternatif::where('id',$id)->first();
        $data->nama = request('nama');
        $data->save();
        $delete =  NilaiAlternatif::where('id_alternatif', $data->id)->delete();
        foreach (request('nilai') as $key => $value) {
            $data_nilai = new NilaiAlternatif;
            $data_nilai->id_alternatif = $data->id;
            $data_nilai->id_sub_kriteria = $value;
            $data_nilai->save();
        }
        return redirect()->route('alternatif.index')->with('success',
        'Berhasil mengubah data');
    }
    public function destroy($id)
    {
        $data = Alternatif::where('id', $id)->first();
        if($data){
            $data->delete();
        }
        return redirect()->route('alternatif.index')->with('danger',
        'Berhasil menghapus data');
    }
}
