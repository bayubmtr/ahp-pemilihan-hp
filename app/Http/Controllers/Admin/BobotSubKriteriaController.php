<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Models\Kriteria;
use App\Models\SubKriteria;
use App\Models\BobotSubKriteria;

class BobotSubKriteriaController extends BobotKriteriaController
{
    public function show($id_kriteria)
    {
        $kriteria = Kriteria::pluck('nama', 'id')->toArray();
        $data = $this->bobot_sub_kriteria($id_kriteria);
        $data['kriteria'] = $kriteria;
        return view('bobot_sub_kriteria.nilai_bobot_sub_kriteria')->with($data);
    }

    protected function bobot_sub_kriteria($id_kriteria)
    {
        $subKriteria = SubKriteria::where('id_kriteria', $id_kriteria)
                ->pluck('nama', 'id')->toArray();
        $bobot = BobotSubKriteria::with('subKriteria1', 'subKriteria2')
                ->whereHas('subKriteria1', function($q) use($id_kriteria){
                    $q->where('id_kriteria', $id_kriteria);
                })->whereHas('subKriteria2', function($q) use($id_kriteria){
                    $q->where('id_kriteria', $id_kriteria);
                })->get();

        $data = $this->perhitungan($subKriteria, $bobot);

        return $data;
    }

    public function index()
    {
        $kriteria = Kriteria::get();
        return view('bobot_sub_kriteria.menu')
        ->with(
            [
                'kriteria' => $kriteria
            ]);
    }

    public function store(Request $request)
    {
        $this->validate($request,[
            'id_sub_kriteria_1' => 'required|exists:sub_kriteria,id',
            'id_sub_kriteria_2' => 'required|exists:sub_kriteria,id',
            'nilai' => 'required',
         ]);

         if ($request->id_sub_kriteria_1 != $request->id_sub_kriteria_2) {
            BobotSubKriteria::updateOrCreate(
                [
                    'id_sub_kriteria_1' => request('id_sub_kriteria_1'),
                    'id_sub_kriteria_2' => request('id_sub_kriteria_2')
                ],
                ['nilai' => request('nilai')]
            );
            BobotSubKriteria::updateOrCreate(
                [
                    'id_sub_kriteria_1' => request('id_sub_kriteria_2'),
                    'id_sub_kriteria_2' => request('id_sub_kriteria_1')
                ],
                ['nilai' => 1/request('nilai')]
            );

        }
        $subKriteria = SubKriteria::where('id', request('id_sub_kriteria_1'))->select('id_kriteria')->first();

        return redirect()->route('bobot_sub_kriteria.show', $subKriteria->id_kriteria)
        ->with('success', 'Berhasil mengubah bobot sub kriteria');
    }

    public function edit($id)
    {
        $data = explode(',', $id);
        if(isset($data[0]) && isset($data[1])){
            $data1 = SubKriteria::findOrFail($data[0]);
            $data2 = SubKriteria::findOrFail($data[1]);
            return view('bobot_sub_kriteria.ubah_bobot_sub_kriteria', compact('data1', 'data2'));
        }
        return redirect()->route('bobot_sub_kriteria.index');
    }
}
