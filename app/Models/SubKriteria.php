<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Model;
class SubKriteria extends Model
{
    protected $fillable = [
        'id', 'nama'
    ];
    protected $table = "sub_kriteria";
    public $timestamps = false;
    public function kriteria()
    {
        return $this->belongsTo('App\Models\Kriteria', 'id_kriteria', 'id');
    }
}
