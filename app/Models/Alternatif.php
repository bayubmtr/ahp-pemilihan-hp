<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Model;
class Alternatif extends Model
{
    protected $fillable = [
        'nama'
    ];
    protected $table = "alternatif";
    public function nilai_alternatif()
    {
        return $this->hasMany('App\Models\NilaiAlternatif', 'id_alternatif', 'id');
    }
}
