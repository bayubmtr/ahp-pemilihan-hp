<?php
use App\Http\Controllers\Admin\HomeController;
use App\Http\Controllers\Admin\AkunController;
use App\Http\Controllers\Admin\KriteriaController;
use App\Http\Controllers\Admin\SubKriteriaController;
use App\Http\Controllers\Admin\HasilController;
use App\Http\Controllers\Admin\AlternatifController;
use App\Http\Controllers\Admin\BobotKriteriaController;
use App\Http\Controllers\Admin\BobotSubKriteriaController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Auth::routes();
Route::group(['middleware' => 'auth'], function()
{
    Route::get('/', [HomeController::class, 'index'])->name('home');
    Route::resource('akun', AkunController::class);
    Route::resource('kriteria', KriteriaController::class);
    Route::resource('sub_kriteria', SubKriteriaController::class);
    Route::get('/hasil', [HasilController::class, 'hasil'])->name('hasil.index');
    Route::get('/hasil/cetak', [HasilController::class, 'cetak'])->name('hasil.cetak');
    Route::resource('alternatif', AlternatifController::class);
    Route::resource('bobot_kriteria', BobotKriteriaController::class);
    Route::resource('bobot_sub_kriteria', BobotSubKriteriaController::class)->parameters(['bobot_sub_kriteria' => 'id']);
});
