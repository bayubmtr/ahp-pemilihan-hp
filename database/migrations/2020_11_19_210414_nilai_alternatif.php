<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class NilaiAlternatif extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('nilai_alternatif', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('id_alternatif')->unsigned();
            $table->bigInteger('id_sub_kriteria')->unsigned();;

            $table->foreign('id_alternatif')->references('id')->on('alternatif')
                  ->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('id_sub_kriteria')->references('id')->on('sub_kriteria')
                  ->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('nilai_alternatif');
    }
}
